package com.example.recycleviewtwo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter (private val users: MutableList<UserModel>, private val itemOnClick: ItemOnClick): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener{

        fun onBind(){
            val model = users[adapterPosition]
            itemView.imageView.setImageResource(model.image)
            itemView.usernameTextView.text = model.username
            itemView.emailTextView.text = model.email
            itemView.statusTextView.text = model.status
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
           itemOnClick.onClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

}