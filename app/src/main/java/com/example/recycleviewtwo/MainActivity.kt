package com.example.recycleviewtwo

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recyclerview_layout.*

class MainActivity : AppCompatActivity() {

    private val userList = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private val requestCode = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(userList, object : ItemOnClick {
            override fun onClick(position: Int) {
                removeUser(position)
            }
        })
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = adapter

        setData()

        addButton.setOnClickListener {
            val intent = Intent(this, NewUserActivity::class.java)
            startActivityForResult(intent, requestCode)
        }
    }

    private fun setData() {
        userList.add(
            0,
            UserModel("Ana Rusadze", "ana.rusadze.1@btu.edu.ge", "Student", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel("Nika Tabatadze", "nikatabatadze9@gmail.com", "Lecturer", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel("Sandro Kakhetelidze", "thesandro1998@gmail.com", "Mentor", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel("Lika Ghlonti", "lika.ghlonti.1@btu.edu.ge", "Student", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel(
                "Ana Morchiladze",
                "ana.morchiladze.1@btu.edu.ge",
                "Student",
                R.mipmap.profile
            )
        )
        userList.add(
            0,
            UserModel(
                "Beka Metreveli",
                "bekametreveliofficial@gmail.com",
                "Student",
                R.mipmap.profile
            )
        )
        userList.add(
            0,
            UserModel("Lika Diasamidze", "Likadiasa@gmail.com", "Student", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel("saba liklikadze", "saba.liklikadze1@gmail.com", "Student", R.mipmap.profile)
        )
        userList.add(
            0,
            UserModel(
                "Giga Sulkhanishvili",
                "giga.sulkhanishvili.1@btu.edu.ge",
                "Student",
                R.mipmap.profile
            )
        )
        userList.add(
            0,
            UserModel(
                "Davit Beriashvili",
                "dato.beriashvili@gmail.com",
                "Student",
                R.mipmap.profile
            )
        )
        userList.add(
            0,
            UserModel(
                "Giorgi Natsvlishvili",
                "giorgihnatsvlishvili@gmail.com",
                "Student",
                R.mipmap.profile
            )
        )
        userList.add(
            0,
            UserModel("Vazha Araviashvili", "17200143@ibsu.edu.ge", "Student", R.mipmap.profile)
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == requestCode) {
            val email = data!!.extras!!.getString("email")
            val username = data.extras!!.getString("username")
            val status = data.extras!!.getString("status")

            userList.add(
                0,
                UserModel(username!!, email!!, status!!, R.mipmap.profile)
            )
            adapter.notifyItemInserted(0)
            recycleView.scrollToPosition(0)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    //
    private fun removeUser(position: Int) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Remove User")
        alert.setMessage("Are you sure you want to remove user?")
        alert.setPositiveButton(
            "Yes"
        ) { _: DialogInterface, _: Int ->
            userList.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
        alert.setNegativeButton("No") { _: DialogInterface, _: Int ->
        }
        alert.show()
    }

}
