package com.example.recycleviewtwo

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_new_user.*
import kotlinx.android.synthetic.main.item_recyclerview_layout.*

class NewUserActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)
        init()
    }

    private fun init() {
        saveButton.setOnClickListener {
            if(usernameEditText.text.isNotEmpty() && emailEditText.text.isNotEmpty() && statusEditText.text.isNotEmpty()){
                userInfo()
            }else{
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun userInfo() {
        intent.putExtra("username", usernameEditText.text.toString())
        intent.putExtra("email", emailEditText.text.toString())
        intent.putExtra("status", statusEditText.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}




