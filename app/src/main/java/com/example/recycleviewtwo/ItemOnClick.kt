package com.example.recycleviewtwo

interface ItemOnClick {
    fun onClick(position:Int)
}